.PHONY: up do ex

SHELL = /bin/sh

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

rebuild:
	docker build --no-cache -t ulco-gildedrose .

build:
	docker build -t ulco-gildedrose .

up:
	docker run -it -d --env-file ./docker/xdebug.env --name ulco-gildedrose -v ${CURDIR}:/app -v ${CURDIR}/docker/xdebug.ini:/usr/local/etc/php/conf.d/docker-php-ext-xdebug-20.ini:ro -w=/app ulco-gildedrose
	docker exec -it ulco-gildedrose sh ./docker/setup-xdebug.sh
	docker exec -it ulco-gildedrose composer install

do:
	docker rm -vf ulco-gildedrose

ex:
	docker exec -u $(CURRENT_UID):$(CURRENT_GID) -it ulco-gildedrose sh
