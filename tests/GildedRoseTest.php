<?php

declare(strict_types=1);

namespace Ulco\Tests;

use Ulco\Item;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function testItemHasSellIn(): void
    {
        $items = [new Item('foo', 4, 5)];

        self::assertEquals(4, $items[0]->sellIn);
    }

    public function testItemHasQuality(): void
    {
        $items = [new Item('foo', 4, 5)];

        self::assertEquals(5, $items[0]->quality);
    }
}